import React, { Component } from 'react';
import './App.css';
import logo from './doticon.svg'
import 'react-mdl/extra/material.css';
import 'react-mdl/extra/material.js';
import { Button, Card, CardText, CardTitle, CardActions, IconButton, FABButton, Icon, Tooltip, Textfield } from 'react-mdl';

const guests = [
  {
    first_name: "Keith",
    last_name: "Cook",
    company: "Skippad"
  },
  {
    first_name: "Dorothy",
    last_name: "Kelly",
    company: "Linkbridge"
  }
  ,{
    first_name: "Louise",
    last_name: "Wilson",
    company: "Agimba"
 },
 {
    first_name: "Barbara",
    last_name: "Cooper",
    company: "Jaxspan"
 },
 {
    first_name: "Carlos",
    last_name: "Warren",
    company: "Chatterpoint"
 },
 {
    first_name: "Kathy",
    last_name: "Lopez",
    company: "Plambee"
 },
 {
    first_name: "Nicole",
    last_name: "Smith",
    company: "Wordify"
 }
];

localStorage.setItem('guests', JSON.stringify(guests));

function searchingFor(searchKeyWord) {
  return function(x){
    return x.first_name.toLowerCase().includes(searchKeyWord.toLowerCase()) ||
     x.last_name.toLowerCase().includes(searchKeyWord.toLowerCase()) || 
     x.company.toLowerCase().includes(searchKeyWord.toLowerCase());
  }

}

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      guests: [],
      searchKeyWord : '',
      details : [],
      show: false,
      numberofGuests : guests.length,
      email: '',
      number: '',
    }

    this.searchHandler = this.searchHandler.bind(this);
    this.viewFullInfor = this.viewFullInfor.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  /*   componentDidMount() {
    I am struggling on how to use the XML data : or How to convert it to JSON
    axios.get("https://gist.githubusercontent.com/eMatsiyana/e315b60a2930bb79e869b37a6ddf8ef1/raw/10c057b39a4dccbe39d3151be78c686dcd1101aa/guestlist.xml")
      .then(res => {
        const guests = res.data;
        this.setState({ guests });
      })
  }
  */
  componentWillMount() {
     this.getGuests();
  }
  getGuests() {
    const guests = JSON.parse(localStorage.getItem('guests'));
    this.setState({ guests });
  }
  searchHandler(event) {
    this.setState({ searchKeyWord : event.target.value })
  }

  handleSubmit(event) {
      event.preventDefault();
      console.log(this.state.email + this.state.number);
  }
  
  viewFullInfor(guests) {
      this.setState({
      details: guests,
      show: true
    });
  }

  render() {
    const { searchKeyWord, guests } = this.state;
    const guestList =  this.state.guests.filter(searchingFor(searchKeyWord)).map(guests => {
      return <li onClick={this.viewFullInfor.bind(this, guests)} key={guests.first_name} className={guests.first_name}>
        <img src={logo} className="img clear" alt="logo" />
        <div className="about-container">
        <div className="name clear">{guests.first_name} {guests.last_name}</div>
        </div>
        </li>
    })
    return (
    <div className="container">
      <div className="side-bar">
       <div className="search">
           <input type="text" placeholder="Search guestlist" onChange={this.searchHandler}  value={searchKeyWord}/>
       </div>
       <h4 style={{ margin: '12px 20px', fontSize: '20px'}}>{this.state.numberofGuests} confirmed guests</h4>
       <div className="list">
           <ul>
            {guestList}
           </ul>
       </div>
     </div>
     <div className="right-bar">
     {
      this.state.show &&
      <Card shadow={0} style={{width: '420px', margin: '30px'}}>
      <CardTitle style={{color: '#000', height: '50px', fontSize: '20px' }}>{this.state.details.first_name} {this.state.details.last_name}</CardTitle>
        <CardText style={{color: '#000', marginBottom: '10px ' }}>
           {this.state.details.company}
        </CardText>
      <CardActions border />
      <CardText style={{color: '#000' }}>
           Contact details :
        </CardText>
          <form style={{ margin: '18px'}}  onSubmit={this.handleSubmit}>
          <Textfield style={{ color: '#000'}}
          label="Email Address"
          pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"
          autoComplete="off"
          value={this.state.name} onChange={e => this.setState({ email: e.target.value })}
          floatingLabel
          style={{width:'100%', color: '#000'}}
          /><br/>
          <Textfield
          pattern="-?[0-9]*(\.[0-9]+)?"
          autoComplete="off"
          maxLength={10}
          value={this.state.name} onChange={e => this.setState({ number: e.target.value })}
          error="Input is not a number!"
          label="Number..."
          floatingLabel
          style={{width: '100%', color: '#000'}}
          />
          <br />
          <Button raised colored>Save</Button>
          </form>
          
      <CardActions border>
        <FABButton mini style={{float: 'right' }} >
        <Tooltip label="Add number" position="left">
            <Icon name="add"></Icon>
      </Tooltip>
      </FABButton>
      </CardActions>
      </Card>
     }
     </div>
   </div>

    );
  }
}

export default App;
